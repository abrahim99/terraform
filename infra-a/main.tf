terraform {
  required_providers {
    local = {
      source = "hashicorp/local"
      version = "2.5.1"
    }
  }
}

provider "local" {
  # Configuration options
}

variable message {
 default = "toto" 
}

locals {
   fname = tomap({ 
       foo1 = "toto1"
       foo2 = "toto2"
       foo3 = "toto3"
   })
}

resource "local_file" "foo" {

  for_each = local.fname
  content  = "${each.value}\n"
  filename = "${path.module}/${each.key}.bar"
  file_permission = "0664"
}

resource "local_file" "foo2" {
  content  = "foo2"
  filename = "${path.module}/foo2.bar"
}

output "msg" {

 value = var.message

 #value = { for k, v in local.fname : k => v.id  }

}
