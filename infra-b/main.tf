terraform {
  required_providers {
    local = {
      source = "hashicorp/local"
      version = "2.5.1"
    }
  }
}

provider "local" {
  # Configuration options
}


data "local_file" "foo" {
      filename = "${path.module}/../infra-a/foo.bar"
}

output "infra-b"  {

    value = data.local_file.foo.content

}
