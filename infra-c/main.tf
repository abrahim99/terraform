terraform {
  required_providers {
    local = {
      source = "hashicorp/local"
      version = "2.5.1"
    }
  }
}

provider "local" {
  # Configuration options
}


data "terraform_remote_state" "infra-b" {
    backend = "local" 
    config = {
      path = "${path.module}/../infra-b/terraform.tfstate" 
    }
}

output "infra-c"  {

    value = data.terraform_remote_state.infra-b.outputs

}
