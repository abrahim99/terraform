include {
  path = find_in_parent_folders()
}
dependency "vpc" {
  config_path = "../vpc"
}
dependency "redis" {
  config_path = "../redis"
}
dependency "mysql" {
  config_path = "../mysql"
}
inputs = {
  vpc_id = dependency.vpc.outputs.vpc_id
  redis_url = dependency.redis.outputs.redis_url
  mysql_host = dependency.mysql.outputs.mysql_host
}
