
include {
  path = find_in_parent_folders()
}
dependency "vpc" {
  config_path = "../vpc"
}
dependency "backend" {
  config_path = "../backend"
}
inputs = {
  vpc_id = dependency.vpc.outputs.vpc_id
  backend_url = dependency.backend.outputs.backend_url
}
