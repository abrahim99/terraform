
variable "vpc_id" { }

output "redis_url" {
  value = "https://redis-${var.vpc_id}"
}
