
variable "vpc_id" {}

output "mysql_username" {
  value = "mysql-username"
}
output "mysql_password" {
  value = "mysql-password"
}

output "mysql_host" {
  value = "https://mysql-${var.vpc_id}"
}
