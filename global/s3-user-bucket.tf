#
# terraform apply   -var user_name=brahim01 -var s3_bucket_name=brahim01-s3
#

variable "s3_bucket_name" {
  type = string
  default = "brahim-s3-bucket"
}

variable "user_name" {
  type = string
  default = "brahim_user"
}

module "user2" {
  source = "https://gitlab.com/abrahim99/modules"
  name                              = var.user_name
  force_destroy                     = true
  pgp_key                           = "keybase:walidportago"
}

#resource "aws_iam_user" "b-user" {
#  name = var.user_name
#}

resource "aws_s3_bucket" "b-s3" {
  bucket = var.s3_bucket_name
  tags = {
    Name        = "Bucket Brahim"
    Environment = "DevTerraform"
  }
}

#resource "aws_s3_bucket_policy" "bucket_policy" {
#  bucket = aws_s3_bucket.b-s3.id
#  policy = jsonencode({
#    Version = "2012-10-17"
#    Statement = [
#      {
#        Effect = "Allow"
#        Principal = {
#          AWS = "${aws_iam_user.b-user.name}"
#        }
#        Action = [
#          "s3:*"
#        ]
#        Resource = [
#          "${aws_s3_bucket.b-s3.arn}/*",
#          "${aws_s3_bucket.b-s3.arn}"
#        ]
#      }
#    ]
#  })
#}
 
#resource "aws_iam_user_policy_attachment" "iam_policy" {
#  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
#  user       = aws_iam_user.b-user.name
#}
